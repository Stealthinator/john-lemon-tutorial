using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    public PlayerInputs Player;
    float timerstart = 0f;

    // Update is called once per frame
    void Update()
    {
        timerstart += Time.deltaTime;
        if(timerstart > 5f)
        {
            Destroy(gameObject);
        }
    }
}
