using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputs : MonoBehaviour
{
    public float movementspeedbonus = 1f;
    public float turnSpeed = 20f;
    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    public GameObject Bomb;
    public Transform BombSpawner;
    PlayerInput inputAction;
    Vector3 movementInput;
    float sprintTime = 5f;
    bool isSprinting = false;
    public int bombs = 0;
    public string SprintStatus = "Ready";

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    private void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();
        movementInput.x = movementVector.x;
        movementInput.y = movementVector.y;  
    }

     private void OnShift(InputValue movementValue)
    {
        if(isSprinting.Equals(false))
        {
        
        isSprinting = true;
        StartCoroutine(sprintTimer());
        }
        
    }

    IEnumerator sprintTimer()
    {
        movementspeedbonus = 3f;
        SprintStatus = "Sprinting";
        print("sprint start speed:" + movementspeedbonus);
        yield return new WaitForSeconds(sprintTime);

        movementspeedbonus = .70f;
        SprintStatus = "Tired";
        print("sprint end speed:" + movementspeedbonus);
        yield return new WaitForSeconds(3f);

        movementspeedbonus = 1f;
        SprintStatus = "Recovering";
        print("sprint end speed:" + movementspeedbonus);
        yield return new WaitForSeconds(2f);

        SprintStatus = "Ready";
        isSprinting = false;
        print("sprint end speed:" + movementspeedbonus + " Sprint :" + isSprinting);
    }

    private void OnFire(InputValue button)
    {
        if(bombs > 0)
        {
            GameObject bombInstance;
            bombInstance = Instantiate(Bomb, BombSpawner.position, BombSpawner.rotation);
            bombInstance.GetComponent<Rigidbody>().AddForce(BombSpawner.forward * 1000f);
            bombs -= 1;
        }
        
    }

    private void FixedUpdate()
    {
        float horizontal = movementInput.x;
        float vertical = movementInput.y;

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude * movementspeedbonus);
        m_Rigidbody.MoveRotation(m_Rotation);
    }


}