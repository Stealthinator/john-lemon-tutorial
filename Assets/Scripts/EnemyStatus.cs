using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStatus : MonoBehaviour
{
    public int health = 5;
    float damagecooldown = .25f;
    bool invincible = false;
    public GameObject parent;

    void OnTriggerEnter(Collider colObject)
    {
        if(colObject.gameObject.tag == "Bomb")
        {
            if(invincible == false)
            {
            health -= 1;
            Destroy(colObject.gameObject);
            StartCoroutine(IFrameTimer());
            }
            
        }
    }

    IEnumerator IFrameTimer()
    {
        invincible = true;
        yield return new WaitForSeconds(damagecooldown);
        invincible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(health <= 0)
        {
            GameObject.Destroy(parent, 0.1f);
        }
    }
}
