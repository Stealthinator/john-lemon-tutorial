using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Transform[] waypoints;
    public GameObject BaseWayPoint;
    int m_CurrentWaypointIndex;
    Rigidbody rb;
    
    void Awake()
    {
        if(waypoints.Length == 0)
        {
            GameObject waypoint;
            waypoint = Instantiate(BaseWayPoint, transform.position, transform.rotation);
            Transform basePointTransform = waypoint.transform;
            Transform[] newPoints = {basePointTransform};
            waypoints = newPoints;
        }
    }

   void Start()
    {
        rb = GetComponent<Rigidbody>();
        if(waypoints.Length != 0)
        {
             navMeshAgent.SetDestination(waypoints[0].position);
        }
    }

    void Update()
    {
        if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
        {
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
        }
    }
}
