using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyControl : MonoBehaviour
{
    
    public List<string> ObtainedKeys;


    void OnTriggerEnter(Collider Object)
    {
        if(Object.tag == "Key")
        {
            ObtainedKeys.Add(Object.name);
            GameObject.Destroy(Object.gameObject);
            print("Keys Collected:" + ObtainedKeys[0]);
        }
    }
    
}
