using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Crown : MonoBehaviour
{
   void OnTriggerEnter(Collider colObject)
   {
       if(colObject.tag == "Player")
       {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex +1);
       }
   }
}
