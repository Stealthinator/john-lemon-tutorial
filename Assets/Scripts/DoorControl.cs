using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorControl : MonoBehaviour
{
    public KeyControl KeyC;
    public string KeyToDoor;
    public GameObject DoorBody;


    void OnTriggerEnter(Collider colliderObject)
    {
        if(colliderObject.tag == "Player" && doesPlayerHaveKey())
        {
            DoorBody.gameObject.SetActive(false);
        }
    }

    void OnTriggerExit(Collider colliderObject)
    {
        if(colliderObject.tag == "Player")
        {
            DoorBody.gameObject.SetActive(true);
        }
    }

    bool doesPlayerHaveKey()
    {
        if(KeyC.ObtainedKeys.Contains(KeyToDoor))
        {
            return true;
        }
        else
        {
            return false;
        }
    }



}
