using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LemonJuice : MonoBehaviour
{
    public PlayerInputs PlayerIn;
    public PlayerStatus PlayerSt;
    public int healpower = 3;
    public int bombincrease = 10;
   void OnTriggerEnter(Collider colObject)
    {
        if(colObject.gameObject.tag == "Player")
        {
            if(PlayerIn.bombs < 100)
            {
                PlayerIn.bombs += bombincrease;
            }
            if(PlayerSt.health < 3)
            {
                PlayerSt.health = healpower;
            }
            Destroy(gameObject);
        }
    }

    void Update()
    {
        transform.Rotate(new Vector3(0, 30, 0) * Time.deltaTime);
    }


}
