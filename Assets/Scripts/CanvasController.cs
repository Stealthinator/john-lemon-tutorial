using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CanvasController : MonoBehaviour
{
    
    public PlayerStatus PStatus;
    public PlayerInputs PInputs;
    public TextMeshProUGUI BombCounterText;
    public TextMeshProUGUI HealthText;
    public TextMeshProUGUI SprintStatus;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        BombCounterText.text = "Lemon Shots: " + PInputs.bombs;
        HealthText.text = "HP: " + PStatus.health;
        SprintStatus.text = "Sprint Status: "+ PInputs.SprintStatus;
        
    }
}
