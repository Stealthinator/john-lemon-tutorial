using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyFollowAi : MonoBehaviour
{
    public GameObject Target = null;
    public float Speed = 25;
    Rigidbody rb;
    public CapsuleCollider EnemyCollider;

    public GameObject BaseWayPoint;
    int m_CurrentWaypointIndex;
    public NavMeshAgent navMeshAgent;
    public Transform[] waypoints;


    void Awake()
    {
        if(waypoints.Length == 0)
        {
            GameObject waypoint;
            waypoint = Instantiate(BaseWayPoint, transform.position, transform.rotation);
            Transform basePointTransform = waypoint.transform;
            Transform[] newPoints = {basePointTransform};
            waypoints = newPoints;
        }
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        if(waypoints.Length != 0)
        {
             navMeshAgent.SetDestination(waypoints[0].position);
        }
    }
  
    //Check waypoints while target is not found, if found set ai to follow
    void Update()
    {
        
        if(Target == null & waypoints.Length != 0)
        {
            navMeshAgent.speed = 2f;
            if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
            {
                m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
                navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
            }
        }
        else if (Target != null)
        {
            navMeshAgent.speed = 2.5f;
            Vector3 pos = Vector3.MoveTowards(transform.position, Target.transform.position, Speed * Time.deltaTime);
            transform.LookAt(Target.gameObject.transform);
            navMeshAgent.SetDestination(pos);
        }
    }
    void OnTriggerEnter(Collider colObject)
    {
        if(colObject.tag == "Player")
        {
            Target = colObject.gameObject;
        }
    }
    
    void OnTriggerExit(Collider colObject)
    {
        if(colObject.tag == "Player")
        {
            Target = null;
        }
    }


}
