using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerStatus : MonoBehaviour
{

    public int health = 5;
    float damagecooldown = 1.5f;
    bool invincible = false;

    void OnTriggerEnter(Collider colObject)
    {
       
        if(colObject.gameObject.tag  == "Enemy")
        {
            if(invincible == false)
            {
            print(health);
            health -= 1;
            print(health);
            StartCoroutine(IFrameTimer());
            }
        }
    }

    IEnumerator IFrameTimer()
    {
        invincible = true;
        yield return new WaitForSeconds(damagecooldown);
        invincible = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(health <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
