using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Miniboss : MonoBehaviour
{
    public int health = 10;
    float damagecooldown = .25f;
    bool invincible = false;
    public GameObject parent;
    public GameObject ghost;
    bool ghostspawned = false;

    void OnTriggerEnter(Collider colObject)
    {
        if(colObject.gameObject.tag == "Bomb")
        {
            if(invincible == false)
            {
            health -= 1;
            Destroy(colObject.gameObject);
            StartCoroutine(IFrameTimer());
            }
            
        }
    }

    IEnumerator IFrameTimer()
    {
        invincible = true;
        yield return new WaitForSeconds(damagecooldown);
        invincible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(health <= 0)
        {
            if(ghostspawned == false)
            {
            GameObject smallGhost;
            ghostspawned = true;
            smallGhost = Instantiate(ghost, gameObject.transform.position, gameObject.transform.rotation);
            }

            GameObject.Destroy(parent, 0.1f);
        }
    }
}
